def calculator(a, b, operator):
    # ==============
    # Your code here
    # Sorry for this really nauseating code but this is the only thing I could come up with!
    # Please tell me how to do this without spamming elifs, I couldn't figure out how to work
    # with the operators as strings.
    # What causes the 0b?
    if operator == "+":
        calculation = a + b
    elif operator == "-":
        calculation = a - b
    elif operator == "*":
        calculation = a * b
    elif operator == "/":
        calculation = a / b
    elif operator != "+" or "-" or "*" or "/":
        calculation = "Please check your input."
    calculation = int(calculation)
    calculation = bin(calculation)
    return calculation[2:]
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
