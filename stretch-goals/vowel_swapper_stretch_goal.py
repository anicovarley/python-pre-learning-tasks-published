def vowel_swapper(string):
    # ==============
    # Your code here
    exception1 = repr("u\/u")
    replacements = {"A": "a/\a", "E": "3", "I": "!", "O": "000", "U": exception1}
    for X in string:
        if X in replacements and string.count("o") == 1:
            replaced_string = string.replace(X, replacements[X])
            string = replaced_string
        elif X in replacements and string.count("o") > 1:
            found = string.rfind("o")
            replaced_string = string.replace(string[found], replacements[X], 1)
            string = replaced_string
    return string
    # I am pretty stuck on this one!
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
