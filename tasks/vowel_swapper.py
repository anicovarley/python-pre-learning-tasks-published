def vowel_swapper(string):
    # ==============
    # Your code here
    # I really don't understand why the second string outputs "H3llooooooooo Wooooooooorld"
    replacement_dict = {"a": "4", "A": "4", "e": "3", "E": "3", "i": "!", "I": "!", "o": "ooo", "O": "000",
                        "u": "|_|", "U": "|_|"}
    for X in string:
        string_replaced = string
        if X in replacement_dict:
            string_replaced = string.replace(X, replacement_dict[X])
            string = string_replaced
    return string_replaced
    # ==============

print(vowel_swapper("aA eE iI oO uU"))  # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World"))  # Should print "H3llooo Wooorld" to the console
print(vowel_swapper("Everything's Available"))  # Should print "3v3ryth!ng's 4v4!l4bl3" to the console
