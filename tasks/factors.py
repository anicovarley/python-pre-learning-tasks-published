def factors(number):
    # ==============
    # Your code here
    integers = [2, 3, 4, 5, 6, 7, 8, 9]
    the_factors = []
    for X in integers[::-1]:
        output = number / X
        if output in integers:
            the_factors.append(int(output))
    return (the_factors)
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “[]” (an empty list) to the console
