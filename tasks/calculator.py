def calculator(a, b, operator):
    # ==============
    # Your code here
    # Sorry for this really nauseating code but this is the only thing I could come up with!
    # Please tell me how to do this without spamming elifs, I couldn't figure out how to work
    # with the operators as strings.
    if operator == "+":
        calculation = a + b
    elif operator == "-":
        calculation = a - b
    elif operator == "*":
        calculation = a * b
    elif operator == "/":
        calculation = a / b
    elif operator != "+" or "-" or "*" or "/":
        calculation = "Please check your input."
    return calculation
    # ==============

print(calculator(2, 4, "+"))
print(calculator(10, 3, "-"))
print(calculator(4, 7, "*"))
print(calculator(100, 2, "/"))